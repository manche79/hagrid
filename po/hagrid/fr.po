#
# Translators:
# Vincent Breitmoser <look@my.amazin.horse>, 2020
# AO <ao@localizationlab.org>, 2021
#
msgid ""
msgstr ""
"Project-Id-Version: hagrid\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-15 16:33-0700\n"
"PO-Revision-Date: 2019-09-27 18:05+0000\n"
"Last-Translator: AO <ao@localizationlab.org>, 2021\n"
"Language-Team: French (https://www.transifex.com/otf/teams/102430/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgid "Error"
msgstr "Erreur"

msgid "Looks like something went wrong :("
msgstr "Il semble qu’un problème est survenu :("

msgid "Error message: {{ internal_error }}"
msgstr "Message d’erreur : {{ internal_error }}"

msgid "There was an error with your request:"
msgstr "Votre demande a généré une erreur :"

msgid "We found an entry for <span class=\"email\">{{ query }}</span>:"
msgstr ""
"Nous avons trouvé une entrée pour <span class=\"email\">{{ query }}</span> :"

msgid ""
"<strong>Hint:</strong> It's more convenient to use <span class=\"brand"
"\">keys.openpgp.org</span> from your OpenPGP software.<br /> Take a look at "
"our <a href=\"/about/usage\">usage guide</a> for details."
msgstr ""
"<strong>Conseil :</strong> Il est plus pratique d’utiliser <span class="
"\"brand\">keys.openpgp.org</span> à partir de votre logiciel OpenPGP.<br /> "
"Vous trouverez plus de précisions dans notre <a href=\"/about/usage\">guide "
"d’utilisation</a>."

msgid "debug info"
msgstr "renseignements de débogage"

msgid "Search by Email Address / Key ID / Fingerprint"
msgstr "Cherchez par adresse courriel / ID de clé / empreinte"

msgid "Search"
msgstr "Chercher"

msgid ""
"You can also <a href=\"/upload\">upload</a> or <a href=\"/manage\">manage</"
"a> your key."
msgstr ""
"Vous pouvez aussi <a href=\"/upload\">téléverser</a> ou <a href=\"/manage"
"\">gérer</a> votre clé."

msgid "Find out more <a href=\"/about\">about this service</a>."
msgstr "En apprendre davantage <a href=\"/about\">sur ce service</a>."

msgid "News:"
msgstr "Nouvelles :"

msgid ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">Celebrating 100.000 "
"verified addresses! 📈</a> (2019-11-12)"
msgstr ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">Nous célébrons 100 "
"000 adresses confirmées ! 📈</a> (12-11-2019)"

msgid "v{{ version }} built from"
msgstr "v{{ version }} compilée à partir de"

msgid "Powered by <a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a>"
msgstr "Propulsé par <a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a>"

msgid ""
"Background image retrieved from <a href=\"https://www.toptal.com/designers/"
"subtlepatterns/subtle-grey/\">Subtle Patterns</a> under CC BY-SA 3.0"
msgstr ""
"L’image d’arrière-plan a été obtenue de <a href=\"https://www.toptal.com/"
"designers/subtlepatterns/subtle-grey/\">Subtle Patterns</a> sous licence CC "
"BY-SA 3.0"

msgid "Maintenance Mode"
msgstr "Mode de maintenance"

msgid "Manage your key"
msgstr "Gérer votre clé"

msgid "Enter any verified email address for your key"
msgstr "Saisissez une adresse courriel confirmée pour votre clé"

msgid "Send link"
msgstr "Envoyer le lien"

msgid ""
"We will send you an email with a link you can use to remove any of your "
"email addresses from search."
msgstr ""
"Nous vous enverrons un courriel avec un lien que vous pourrez utiliser pour "
"supprimer de la recherche l’une de vos adresses courriel."

msgid ""
"Managing the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"Gestion de la clé <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."

msgid "Your key is published with the following identity information:"
msgstr "Votre clé est publiée avec les renseignements d’identité suivants :"

msgid "Delete"
msgstr "Supprimer"

msgid ""
"Clicking \"delete\" on any address will remove it from this key. It will no "
"longer appear in a search.<br /> To add another address, <a href=\"/upload"
"\">upload</a> the key again."
msgstr ""
"Cliquer sur « Supprimer » pour l’une des adresses l’enlèvera de cette clé. "
"Elle n’apparaîtra plus lors d’une recherche.<br /> Pour ajouter une autre "
"adresse, <a href=\"/upload\">téléversez</a> la clé de nouveau."

msgid ""
"Your key is published as only non-identity information.  (<a href=\"/about\" "
"target=\"_blank\">What does this mean?</a>)"
msgstr ""
"Votre clé n’est publiée qu’en tant que renseignement qui ne permet pas de "
"vous identifier. (<a href=\"/about\" target=\"_blank\">Qu’est-ce que cela "
"signifie ?</a>)"

msgid "To add an address, <a href=\"/upload\">upload</a> the key again."
msgstr ""
"Pour ajouter une adresse, <a href=\"/upload\">téléversez</a> la clé de "
"nouveau."

msgid ""
"We have sent an email with further instructions to <span class=\"email"
"\">{{ address }}</span>."
msgstr ""
"Un courriel avec de plus amples instructions a été envoyé à <span class="
"\"email\">{{ address }}</span>."

msgid "This address has already been verified."
msgstr "Cette adresse a déjà été confirmée."

msgid ""
"Your key <span class=\"fingerprint\">{{ key_fpr }}</span> is now published "
"for the identity <a href=\"{{userid_link}}\" target=\"_blank\"><span class="
"\"email\">{{ userid }}</span></a>."
msgstr ""
"Votre clé <span class=\"fingerprint\">{{ key_fpr }}</span> est maintenant "
"publiée pour l’identité <a href=\"{{userid_link}}\" target=\"_blank\"><span "
"class=\"email\">{{ userid }}</span></a>. "

msgid "Upload your key"
msgstr "Téléverser votre clé"

msgid "Upload"
msgstr "Téléverser"

msgid ""
"Need more info? Check our <a target=\"_blank\" href=\"/about\">intro</a> and "
"<a target=\"_blank\" href=\"/about/usage\">usage guide</a>."
msgstr ""
"Besoin de plus de précisions ? Consultez notre <a target=\"_blank\" href=\"/"
"about\">présentation</a> et notre <a target=\"_blank\" href=\"/about/usage"
"\">guide d’utilisation</a>."

msgid ""
"You uploaded the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"Vous avez téléversé la clé <span class=\"fingerprint\"><a href="
"\"{{ key_link }}\" target=\"_blank\">{{ key_fpr }}</a></span>."

msgid "This key is revoked."
msgstr "Cette clé est révoquée."

msgid ""
"It is published without identity information and can't be made available for "
"search by email address (<a href=\"/about\" target=\"_blank\">what does this "
"mean?</a>)."
msgstr ""
"Elle est publiée sans renseignements d’identité et une recherche par adresse "
"courriel ne la trouvera pas (<a href=\"/about\" target=\"_blank\">qu’est-ce "
"que cela signifie ?</a>)."

msgid ""
"This key is now published with the following identity information (<a href="
"\"/about\" target=\"_blank\">what does this mean?</a>):"
msgstr ""
"Cette clé est maintenant publiée avec les renseignements d’identité suivants "
"(<a href=\"/about\" target=\"_blank\">qu’est-ce que cela signifie ?</a>) :"

msgid "Published"
msgstr "Publiée"

msgid ""
"This key is now published with only non-identity information. (<a href=\"/"
"about\" target=\"_blank\">What does this mean?</a>)"
msgstr ""
"Cette clé est maintenant publiée avec seulement des renseignements qui ne "
"permettent pas de vous identifier. (<a href=\"/about\" target=\"_blank"
"\">Qu’est-ce que cela signifie ?</a>)"

msgid ""
"To make the key available for search by email address, you can verify it "
"belongs to you:"
msgstr ""
"Afin qu’une recherche par adresse courriel trouve cette clé, vous pouvez "
"confirmer qu’elle vous appartient :"

msgid "Verification Pending"
msgstr "La confirmation est en attente"

msgid ""
"<strong>Note:</strong> Some providers delay emails for up to 15 minutes to "
"prevent spam. Please be patient."
msgstr ""
"<strong>Note :</strong> Certains fournisseurs retardent les courriels "
"jusqu’à 15 minutes afin de prévenir les courriels indésirables (pourriels). "
"Veuillez faire preuve de patience."

msgid "Send Verification Email"
msgstr "Envoyer un courriel de confirmation"

msgid ""
"This key contains one identity that could not be parsed as an email address."
"<br /> This identity can't be published on <span class=\"brand\">keys."
"openpgp.org</span>.  (<a href=\"/about/faq#non-email-uids\" target=\"_blank"
"\">Why?</a>)"
msgstr ""
"Cette clé comprend une identité qui n’a pas pu être analysée en tant "
"qu’adresse courriel.<br /> Cette identité ne peut pas être publiée sur <span "
"class=\"brand\">keys.openpgp.org</span>. (<a href=\"/about/faq#non-email-uids"
"\" target=\"_blank\">Pourquoi ?</a>) "

msgid ""
"This key contains {{ count_unparsed }} identities that could not be parsed "
"as an email address.<br /> These identities can't be published on <span "
"class=\"brand\">keys.openpgp.org</span>.  (<a href=\"/about/faq#non-email-"
"uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Cette clé comprend {{ count_unparsed }} identités qui n’ont pas pu être "
"analysées en tant qu’adresse courriel.<br /> Ces identités ne peuvent pas "
"être publiées sur <span class=\"brand\">keys.openpgp.org</span>. (<a href=\"/"
"about/faq#non-email-uids\" target=\"_blank\">Pourquoi ?</a>) "

msgid ""
"This key contains one revoked identity, which is not published. (<a href=\"/"
"about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Cette clé comprend une identité révoquée qui n’est pas publiée. (<a href=\"/"
"about/faq#revoked-uids\" target=\"_blank\">Pourquoi ?</a>)"

msgid ""
"This key contains {{ count_revoked }} revoked identities, which are not "
"published. (<a href=\"/about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Cette clé comprend {{ count_revoked }} identités révoquées qui ne sont pas "
"publiées. (<a href=\"/about/faq#revoked-uids\" target=\"_blank\">Pourquoi ?</"
"a>)"

msgid "Your keys have been successfully uploaded:"
msgstr "Vos clés ont été téléversées avec succès :"

msgid ""
"<strong>Note:</strong> To make keys searchable by email address, you must "
"upload them individually."
msgstr ""
"<strong>Note :</strong> Afin qu’une recherche par adresse courriel trouve "
"des clés, vous devez les téléverser individuellement."

msgid "Verifying your email address…"
msgstr "Confirmation de votre adresse courriel…"

msgid ""
"If the process doesn't complete after a few seconds, please <input type="
"\"submit\" class=\"textbutton\" value=\"click here\" />."
msgstr ""
"Si le processus n’est pas terminé après quelques secondes, veuillez <input "
"type=\"submit\" class=\"textbutton\" value=\"cliquer ici\"/>."

msgid "Manage your key on {{domain}}"
msgstr "Gérer votre clé sur {{domain}}"

msgid "Hi,"
msgstr "Bonjour,"

msgid ""
"This is an automated message from <a href=\"{{base_uri}}\" style=\"text-"
"decoration:none; color: #333\">{{domain}}</a>."
msgstr ""
"Ceci est un courriel automatisé de <a href=\"{{base_uri}}\" style=\"text-"
"decoration:none; color: #333\">{{domain}}</a>."

msgid "If you didn't request this message, please ignore it."
msgstr "Si vous n’avez pas demandé ce courriel, veuillez l’ignorer."

msgid "OpenPGP key: <tt>{{primary_fp}}</tt>"
msgstr "Clé OpenPGP : <tt>{{primary_fp}}</tt>"

msgid ""
"To manage and delete listed addresses on this key, please follow the link "
"below:"
msgstr ""
"Pour gérer et supprimer les adresses répertoriées de cette clé, veuillez "
"suivre le lien ci-dessous :"

msgid ""
"You can find more info at <a href=\"{{base_uri}}/about\">{{domain}}/about</"
"a>."
msgstr ""
"Pour de plus amples renseignements, consultez <a href=\"{{base_uri}}/about"
"\">{{domain}}/about</a>."

msgid "distributing OpenPGP keys since 2019"
msgstr "distribue des clés OpenPGP depuis 2019"

msgid "This is an automated message from {{domain}}."
msgstr "Ceci est un courriel automatisé de {{domain}}."

msgid "OpenPGP key: {{primary_fp}}"
msgstr "Clé OpenPGP : {{primary_fp}}"

msgid "You can find more info at {{base_uri}}/about"
msgstr "Pour de plus amples renseignements, consultez {{base_uri}}/about"

msgid "Verify {{userid}} for your key on {{domain}}"
msgstr "Confirmer {{userid}} pour votre clé sur {{domain}}"

msgid ""
"To let others find this key from your email address \"<a rel=\"nofollow\" "
"href=\"#\" style=\"text-decoration:none; color: #333\">{{userid}}</a>\", "
"please click the link below:"
msgstr ""
"Afin de permettre à d’autres de trouver cette clé à partir de votre adresse "
"courriel « <a rel=\"nofollow\" href=\"#\" style=\"text-decoration:none; "
"color : #333\">{{userid}}</a> », veuillez cliquer sur le lien ci-dessous :"

msgid ""
"To let others find this key from your email address \"{{userid}}\",\n"
"please follow the link below:"
msgstr ""
"Afin de permettre à d’autres de trouver cette clé à partir de votre\n"
"adresse courriel « {{userid}} », veuillez suivre le lien ci-dessous :"

msgid "No key found for fingerprint {}"
msgstr "Aucune clé n’a été trouvée pour l’empreinte {}"

msgid "No key found for key id {}"
msgstr "Aucune clé n’a été trouvée pour l’ID de clé {}"

msgid "No key found for email address {}"
msgstr "Aucune clé n’a été trouvée pour l’adresse courriel {}"

msgid "Search by Short Key ID is not supported."
msgstr "La recherche par ID de clé courte n’est pas prise en charge."

msgid "Invalid search query."
msgstr "La requête d’interrogation est invalide."

msgctxt "Subject for verification email, {0} = userid, {1} = keyserver domain"
msgid "Verify {0} for your key on {1}"
msgstr "Confirmer {0} pour votre clé sur {1}"

msgctxt "Subject for manage email, {} = keyserver domain"
msgid "Manage your key on {}"
msgstr "Gérer votre clé sur {}"

msgid "This link is invalid or expired"
msgstr "Ce lien est invalide ou expiré"

#, fuzzy
msgid "Malformed address: {}"
msgstr "Cette adresse est malformée :"

#, fuzzy
msgid "No key for address: {}"
msgstr "Il n’y a pas de clé pour cette adresse : {address}"

msgid "A request has already been sent for this address recently."
msgstr "Une demande a déjà été envoyée récemment pour cette adresse."

msgid "Parsing of key data failed."
msgstr "Échec d’analyse des données de la clé."

msgid "Whoops, please don't upload secret keys!"
msgstr "Attention : Veuillez ne pas téléverser de clés secrètes !"

msgid "No key uploaded."
msgstr "Aucune clé n’a été téléversée."

msgid "Error processing uploaded key."
msgstr "Erreur de traitement de la clé téléversée."

msgid "Upload session expired. Please try again."
msgstr "La session de téléversement est expirée. Veuillez ressayer."

msgid "Invalid verification link."
msgstr "Le lien de confirmation est invalide."
